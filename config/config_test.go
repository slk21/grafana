package config

import (
	"testing"
)

func Test_Load(t *testing.T) {
	err := Load("/home/vyacheslav/go/src/studentgit.kata.academy/SLK/service/.env")
	if err != nil {
		t.Errorf("Ошибка при загрузке файла .env: %v", err)
	}
}
