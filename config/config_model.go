package config

import (
	"os"
)

// AppConf структура конфигурации приложения
type AppConf struct {
	Server      Server
	TokenSecret string
	DB          DB
	Redis       Redis
}

// Server конфигурация для запуска сервера
type Server struct {
	Port string
	Host string
}

// DB конфигурация для подключения к БД
type DB struct {
	Name     string
	User     string
	Password string
	Host     string
	Port     string
	Driver   string
}

type Redis struct {
	Host string
	Port string
}

func NewAppConf() AppConf {
	Load("../.env")
	return AppConf{
		Server: Server{
			Port: os.Getenv("SERVER_PORT"),
			Host: os.Getenv("SERVER_HOST"),
		},
		TokenSecret: os.Getenv("TOKEN_SECRET"),
		DB: DB{
			Name:     os.Getenv("DB_NAME"),
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			Driver:   os.Getenv("DB_DRIVER"),
		},
		Redis: Redis{
			Host: os.Getenv("REDIS_HOST"),
			Port: os.Getenv("REDIS_PORT"),
		},
	}
}
