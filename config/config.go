package config

import (
	"fmt"
	"github.com/joho/godotenv"
)

// Load загркзка данных из .env в environment
func Load(path string) error {

	err := godotenv.Load(path)
	if err != nil {
		fmt.Println("Ошибка загрузки файла .env")
		return err
	}

	return nil
}
