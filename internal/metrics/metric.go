package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
	"time"
)

func MeasureDuration(handlerName string, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		// Увеличиваем счетчик количества вызовов обработчика
		HttpRequestsTotal.WithLabelValues(handlerName).Inc()

		defer func() {
			// Измеряем время работы обработчика
			duration := time.Since(start).Seconds()
			HandlerDurationSeconds.WithLabelValues(handlerName).Observe(duration)
		}()

		next(w, r)
	}
}

var (
	// Счетчики для измерения количества вызовов каждого обработчика
	HttpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests.",
		},
		[]string{"handler"},
	)

	// Гистограммы для измерения времени работы каждого обработчика
	HandlerDurationSeconds = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "handler_duration_seconds",
			Help:    "Duration of handler execution in seconds.",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"handler"},
	)
)

// Гистограммы для измерения времени запросов в БД
var BdDurationSeconds = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "postgres_duration_seconds",
		Help:    "Duration of postgres execution in seconds.",
		Buckets: prometheus.DefBuckets,
	},
	[]string{"type_request_postgres"},
)

// Гистограммы для измерения времени запросов в Redis
var RedisDurationSeconds = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "redis_duration_seconds",
		Help:    "Duration of redis execution in seconds.",
		Buckets: prometheus.DefBuckets,
	},
	[]string{"type_request_redis"},
)

// Гистограммы для измерения времени запросов в Redis
var DadataDurationSeconds = prometheus.NewHistogramVec(
	prometheus.HistogramOpts{
		Name:    "dadata_duration_seconds",
		Help:    "Duration of redis execution in seconds.",
		Buckets: prometheus.DefBuckets,
	},
	[]string{"type_request_redis"},
)
