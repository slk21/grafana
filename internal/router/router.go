package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
	"net/http/pprof"
	"studentgit.kata.academy/SLK/repository/internal/infrastructure/proxy"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	midd "studentgit.kata.academy/SLK/repository/internal/middleware"
	"studentgit.kata.academy/SLK/repository/internal/models"
	userController "studentgit.kata.academy/SLK/repository/pkg/user/controller"
)

// Router Функция отвечает за создание роутера и его маршрутов
func Router(port string) http.Handler {

	r := chi.NewRouter() // Создание нового роутера
	r.Use(midd.Middleware)
	// маршрут документации
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:"+port+"/swagger/doc.json"), //The url pointing to API definition
	))
	// маршрут метрики
	r.Handle("/metrics", promhttp.Handler())
	// маршруты пользователя
	r.Post("/user", metrics.MeasureDuration("create_user", userController.CreateUser))
	r.Route("/user/", func(r chi.Router) {
		r.Post("/createWithList", metrics.MeasureDuration("create_with_list", userController.CreateWithList))
		r.Get("/{username}", metrics.MeasureDuration("get_by_username", userController.GetByUsername))
		r.Put("/{username}", metrics.MeasureDuration("update_by_username", userController.UpdateByUsername))
		r.Delete("/{username}", metrics.MeasureDuration("delete_by_username", userController.DeleteByUsername))
		r.Get("/login", metrics.MeasureDuration("login", userController.Login))
		r.Post("/createWithArray", metrics.MeasureDuration("create_with_array", userController.CreateWithArray))
	})

	// группа защищенных маршрутов, требующих авторизации для адресов
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(models.TokenAuth))
		r.Use(jwtauth.Authenticator(models.TokenAuth))
		r.Post("/api/address/search", metrics.MeasureDuration("search_address", proxy.Proxy))
		r.Post("/api/address/geocode", metrics.MeasureDuration("geocode_address", proxy.Proxy))
	})

	r.Route("/mycustompath/", func(r chi.Router) {
		r.Use(jwtauth.Verifier(models.TokenAuth))
		r.Use(jwtauth.Authenticator(models.TokenAuth))
		r.Get("/pprof", pprof.Index)
		r.Get("/trace", pprof.Trace)
		r.Get("/symbol", pprof.Symbol)
		r.Get("/cmdline", pprof.Cmdline)
		r.Get("/profile", pprof.Profile)
		r.Get("/heap", pprof.Handler("heap").ServeHTTP)
		r.Get("/block", pprof.Handler("block").ServeHTTP)
		r.Get("/mutex", pprof.Handler("mutex").ServeHTTP)
		r.Get("/allocs", pprof.Handler("allocs").ServeHTTP)
		r.Get("/goroutine", pprof.Handler("goroutine").ServeHTTP)
		r.Get("/threadcreate", pprof.Handler("threadcreate").ServeHTTP)
	})

	return r
}
