package middleware

import (
	"log"
	"net/http"
	"strings"
)

// Middleware проверка URL на соответствие
func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, "/user/") || r.URL.Path == "/user" {
			next.ServeHTTP(w, r)
			return
		} else if strings.Contains(r.URL.Path, "/swagger/") || r.URL.Path == "/pet" {
			next.ServeHTTP(w, r)
			return
		} else if strings.Contains(r.URL.Path, "/api/address") || strings.Contains(r.URL.Path, "/mycustompath/") {
			next.ServeHTTP(w, r)
			return
		} else if r.URL.Path == "/metrics" {
			next.ServeHTTP(w, r)
			return
		}
		log.Println("сработал middleware")
		http.NotFound(w, r)
	})
}
