package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

type testCase struct {
	url             string
	expectedCode    int
	expectedRequest string
}

func Test_middlewares(t *testing.T) {

	testsi := []testCase{
		{
			url:             "/api/address/search",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/api/address/geocode",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/",
			expectedCode:    404,
			expectedRequest: "404 page not found\n",
		},
		{
			url:             "/api/login",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
		{
			url:             "/api/register",
			expectedCode:    200,
			expectedRequest: "TEST",
		},
	}

	for _, val := range testsi {
		req := httptest.NewRequest(http.MethodGet, val.url, nil)

		rr := httptest.NewRecorder()

		hadler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write([]byte("TEST"))
			if err != nil {
				t.Fatal(err)
			}
		})

		Middleware(hadler).ServeHTTP(rr, req)

		if val.expectedCode != rr.Code {
			t.Errorf("Ожидал: %v \n Получил: %v", val.expectedCode, rr.Code)
		}

		if val.expectedRequest != rr.Body.String() {
			t.Errorf("Ожидал: %s \n Получил: %s", val.expectedRequest, rr.Body.String())
		}
	}

}
