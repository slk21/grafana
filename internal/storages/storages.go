package storages

import (
	"database/sql"
	"studentgit.kata.academy/SLK/repository/config"
	"studentgit.kata.academy/SLK/repository/pkg/db/postgres"
	red "studentgit.kata.academy/SLK/repository/pkg/db/redis"
)

var PostgreSQL *sql.DB

var Prox red.Cacher

func NewStorages(confDb config.DB) {
	var err error
	PostgreSQL, err = postgres.OpenDB(confDb)
	if err != nil {
		panic(err)
	}
}
