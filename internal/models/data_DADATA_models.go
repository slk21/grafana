package models

// Структура для получения ответа от dadata.ru
type Address struct {
	City   string `json:"city"`
	Street string `json:"street"`
	House  string `json:"house"`
	Lat    string `json:"geo_lat"`
	Lon    string `json:"geo_lon"`
}

// Структура для хранения множества ответов от dadata.ru на запрос по поиску инофрмации о фактическом адресе

type SearchResponse struct {
	Addresses []Address `json:"addresses"`
}
