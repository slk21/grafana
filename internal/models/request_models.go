package models

type SearchRequest struct {
	Query string `json:"query"`
}

type GeoCodeRequest struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}
