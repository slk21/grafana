package models

import (
	"net/http"
	"time"
)

func Standart_Header(w http.ResponseWriter) {
	curenTime := time.Now()
	dateString := curenTime.Format("Mon, 02 Jan 2006 15:04:05 GMT")

	w.Header().Set("access-control-allow-headers", "Content-Type,api_key,Authorization")
	w.Header().Set("access-control-allow-methods", "GET,POST,DELETE,PUT")
	w.Header().Set("access-control-allow-origin", "*")
	w.Header().Set("content-type", "application/json")
	w.Header().Set("date", dateString)
}
