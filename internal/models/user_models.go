package models

// User представление пользователя в памяти
type User struct {
	Id         int64  `json:"id"`
	Username   string `json:"username"`
	Firstname  string `json:"firstname"`
	Lastname   string `json:"lastname"`
	Email      string `json:"email"`
	UserStatus int32  `json:"user_status"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
}
