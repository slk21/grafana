package server

import (
	"errors"
	"log"
	"net/http"
	_ "net/http/pprof"
	"studentgit.kata.academy/SLK/repository/config"
	rout "studentgit.kata.academy/SLK/repository/internal/router"
	"time"
)

type Server interface {
	Serve() error
	Shutdown() *http.Server
}

type HttpServer struct {
	conf      config.Server
	configTok string
	srv       *http.Server
}

func NewHttpServer(conf config.AppConf) Server {
	return &HttpServer{conf: conf.Server, configTok: conf.TokenSecret, srv: &http.Server{
		Addr:         ":" + conf.Server.Port,
		Handler:      rout.Router(conf.Server.Port),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	},
	}
}

func (s *HttpServer) Serve() error {
	log.Printf("Запуск REST сервера на: %s:%s.", s.conf.Host, s.conf.Port)

	// Запуск сервера в отдельной горутине
	go func() {
		if err1 := s.srv.ListenAndServe(); err1 != nil && !errors.Is(err1, http.ErrServerClosed) {
			log.Fatalf("Ошибка при запуске сервера: %v", err1)
		}
	}()

	return nil
}

func (s *HttpServer) Shutdown() *http.Server {
	return s.srv
}
