package serviceGeo

import (
	"encoding/json"
	"fmt"
	"log"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"time"
)

func checkCache(txt string, s *StoreService) ([]models.Address, error) {
	var res []models.Address

	start := time.Now() // стартовое время для метрики

	sls, err := s.postgers.GetCoincidences(txt)
	if err != nil && sls != nil {
		return nil, err
	} else if sls != nil && len(sls) > 0 {
		resForAddress := []int{}
		for key := range sls {
			resk, errr := s.postgers.GetConnection(key)
			if errr != nil {
				log.Println(errr)
				return nil, errr
			}

			resForAddress = append(resForAddress, resk...)

		}

		adrs, errr := s.postgers.GetAddress(resForAddress)
		if errr != nil {
			log.Println(errr)
			return nil, errr
		}

		res = append(res, adrs...)

		log.Println("Выдано значение из кэша БД postgres")
		return res, nil
	} else if err == nil && sls == nil {
		panic(1)
	}

	duration := time.Since(start).Seconds()                                              // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("CHECK_CACHCE_POSTGRES").Observe(duration) // отправка собранной метрики

	return res, nil
}

func structForRedis(user []models.Address) ([]byte, error) {
	jsonUser, err := json.Marshal(user)
	if err != nil {
		return nil, fmt.Errorf("ошибка при маршализировании структуры User: %v", err)
	}

	return jsonUser, nil
}
