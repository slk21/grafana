package serviceGeo

import "studentgit.kata.academy/SLK/repository/internal/models"

type Georer interface {
	Search(query string) ([]models.Address, error)
	GeoCod(body []byte) ([]models.Address, error)
}
