package serviceGeo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	postgresStore "studentgit.kata.academy/SLK/repository/internal/infrastructure/handlers/geo/storage/postgres"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"studentgit.kata.academy/SLK/repository/internal/storages"
	"studentgit.kata.academy/SLK/repository/pkg/db/redis"
	"time"
)

type StoreService struct {
	postgers postgresStore.StoreRepository
	redis    redis.Cacher
}

func NewGeoService() Georer {
	return &StoreService{
		postgers: postgresStore.NewStoreRepository(storages.PostgreSQL),
		redis:    storages.Prox,
	}
}

func (s *StoreService) GeoCod(body []byte) ([]models.Address, error) {

	// В сервисе должен быть инкодинг в структуру запроса для проверки по кэшу и для помещения значений в кэш

	var request models.GeoCodeRequest

	err := json.Unmarshal(body, &request)
	if err != nil {
		log.Println("Ошибка 1 декодирования данных: ", err)
		return nil, err
	}

	// в случае нахождения в кэше подходящих значений вытаскиваем значений и передаем на отправку клиенту

	resAdrs, err := checkCache(fmt.Sprintf("%.3f_%.3f", request.Lon, request.Lat), s)
	if resAdrs != nil {
		return resAdrs, nil
	}

	// в случае если значений не нашлось отправляем запрос с данными в виде []byte на dadata.r

	start := time.Now() // начало отсчета для метрики

	client := &http.Client{}

	req, err := http.NewRequest("POST", "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", bytes.NewBuffer(body))
	if err != nil {
		log.Println("Ошибка при создание нового запроса на dadata.ru", err)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Token c170e2fd600f4420409a03bb48bb1fdc87b3ae4b")
	req.Header.Set("X-Secret", "7543ca2e6a31a90afd7f3e7bae656925ec802844")

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Ошибка при отправке запроса на dadata.ru", err)
		return nil, err
	}
	defer resp.Body.Close()

	duration := time.Since(start).Seconds()                                           // вычисление работы запроса
	metrics.DadataDurationSeconds.WithLabelValues("DADATA_REQUEST").Observe(duration) // отправка собранной метрики

	var geo models.GeoCode

	err = json.NewDecoder(resp.Body).Decode(&geo)
	if err != nil {
		log.Println("Ошибка 2 декодирования данных: ", err)
		return nil, err
	}

	var res []models.Address

	for _, r := range geo.Suggestions {
		var address models.Address
		address.City = string(r.Data.City)
		address.Street = string(r.Data.Street)
		address.House = r.Data.House
		address.Lat = r.Data.GeoLat
		address.Lon = r.Data.GeoLon

		res = append(res, address)
	}

	key := fmt.Sprintf("%.6f_%.6f", request.Lat, request.Lon)
	// добавление полученных значений в redis
	start = time.Now() // начальное время для метрики

	red, _ := structForRedis(res)
	err = s.redis.Set(key, red)

	duration = time.Since(start).Seconds()                                            // вычисление работы запроса
	metrics.RedisDurationSeconds.WithLabelValues("CREATE_IN_REDIS").Observe(duration) // отправка собранной метрики

	if err != nil {
		log.Println("Ошибка при добавлении ответа от dadata.ru в Redis", err)

	}

	// полученные ответы добавляем в кэш с помощью слоя репозитория
	// добавление в историю поиска значений поиска
	start = time.Now() // стартовое время для метрики

	requestStr := fmt.Sprintf("%.3f_%.3f", request.Lon, request.Lat)
	idSearchHistory, err := s.postgers.CreateSearchHistory(requestStr)
	if err != nil {
		return nil, err
	}

	// добавление полученных ответ на запрос
	sl := []int{}
	for _, adress := range res {
		idAddress, errs := s.postgers.CreateAddress(adress)
		if errs != nil {
			return nil, errs
		}
		sl = append(sl, idAddress)
	}

	// преобразууем полученные значения ID ответов в срез, а затем в строку
	valueStr := "{" + strings.Trim(strings.Join(strings.Fields(fmt.Sprint(sl)), ","), "[]") + "}"

	// добавляем значение ID запроса и ответов на него в связующую таблицу
	err = s.postgers.CreateHistorySearchAddress(idSearchHistory, valueStr)
	if err != nil {
		return nil, err
	}

	duration = time.Since(start).Seconds()                                               // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("CREATE_SEARCH_HISTORY").Observe(duration) // отправка собранной метрики

	// далее отправляем ответ клиенту

	return res, nil
}

func (s *StoreService) Search(query string) ([]models.Address, error) {
	//проверка кэша
	resAdrs, err := checkCache(query, s)
	if resAdrs != nil {
		return resAdrs, nil
	}

	servReq := []string{query}
	servReqJS, err := json.Marshal(servReq)
	if err != nil {
		log.Println("Ошибка кодирования данных: ", err)
		return nil, err
	}

	client := &http.Client{}

	start := time.Now()

	req, err := http.NewRequest("POST", "https://cleaner.dadata.ru/api/v1/clean/address", bytes.NewBuffer(servReqJS))
	if err != nil {
		log.Println("Ошибка при создание нового запроса на dadata.ru", err)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Token c170e2fd600f4420409a03bb48bb1fdc87b3ae4b")
	req.Header.Set("X-Secret", "7543ca2e6a31a90afd7f3e7bae656925ec802844")

	res, err := client.Do(req)

	if err != nil {
		log.Println("Ошибка при отправке запроса на dadata.ru", err)
		return nil, err
	}
	defer res.Body.Close()

	duration := time.Since(start).Seconds()                                           // вычисление работы запроса
	metrics.DadataDurationSeconds.WithLabelValues("DADATA_REQUEST").Observe(duration) // отправка собранной метрики

	bodyReq, err := io.ReadAll(res.Body)
	if err != nil {
		log.Println("Ошибка при чтении данных ответа dadata.ru", err)
	}

	var serachResp models.SearchResponse

	err = json.Unmarshal(bodyReq, &serachResp.Addresses)
	if err != nil {
		log.Println("Ошибка 2 декодирования данных: ", err)
		return nil, err
	}

	// Добавление полученного ответа в кэш redis
	start = time.Now() // начало отсчета для метрики

	red, _ := structForRedis(serachResp.Addresses)
	err = s.redis.Set(query, red)

	duration = time.Since(start).Seconds()                                            // вычисление работы запроса
	metrics.RedisDurationSeconds.WithLabelValues("CREATE_IN_REDIS").Observe(duration) // отправка собранной метрики

	if err != nil {
		log.Println("Ошибка при добавлении ответа от dadata.ru в Redis", err)

	}

	// полученные ответы добавляем в кэш с помощью слоя репозитория
	// добавление в историю поиска значений поиска

	start = time.Now() // стартовое время для метрики

	idSearchHistory, err := s.postgers.CreateSearchHistory(query)
	if err != nil {
		return nil, err
	}

	// добавление полученных ответ на запрос
	sl := []int{}
	idAddress, errs := s.postgers.CreateAddress(serachResp.Addresses[0])
	if errs != nil {
		return nil, err
	}
	sl = append(sl, idAddress)

	// преобразууем полученные значения ID ответов в срез, а затем в строку
	valueStr := "{" + strings.Trim(strings.Join(strings.Fields(fmt.Sprint(sl)), ","), "[]") + "}"

	// добавляем значение ID запроса и ответов на него в связующую таблицу
	err = s.postgers.CreateHistorySearchAddress(idSearchHistory, valueStr)
	if err != nil {
		return nil, err
	}

	duration = time.Since(start).Seconds()                                               // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("CREATE_SEARCH_HISTORY").Observe(duration) // отправка собранной метрики

	// далее отправляем ответ клиенту

	return serachResp.Addresses, nil
}
