package geoController

import (
	"encoding/json"
	"log"
	"net/http"
	serviceStore "studentgit.kata.academy/SLK/repository/internal/infrastructure/handlers/geo/service"
	"studentgit.kata.academy/SLK/repository/internal/models"
)

// GeoCod отправляет запрос на dadata.ru и возвращает полученый ответ клиенту.
// @Summary Получение данных адреса.
// @Security JWTAuth
// @Tags address search
// @Description Поиск информации об адресе по его геолокационным данным.
// @Param input body models.GeoCodeRequest  true "координаты".
// @Success      200  {object}  []models.Address "информация об адресе"
// @Failure      400  {string}  string "400 :Неверный формат запроса"
// @Failure      500  {string}  string "500: Сервис dadata.ru не доступен"
// @Router       /api/address/geocode [post]
func GeoCod(w http.ResponseWriter, req models.GeoCodeRequest) {

	bodyJS, _ := json.Marshal(req)

	// далее отправляем срез на слой сервиса для дальнейшей обработки и получения структуры ответа
	res, err := serviceStore.NewGeoService().GeoCod(bodyJS)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Println("Ошибка кодирования в JSON", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(resJSON)
	if err != nil {
		log.Println("Ошибка отправки ответа: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

// Search отправляет запрос на dadata.ru и возвращает получнный ответ клиенту
// @Summary Получение данных адреса
// @Security JWTAuth
// @Tags address search
// @Description Поиск информации об адресе по его фактическому адресу
// @Param input body models.SearchRequest  true "фактический адрес"
// @Success      200  {object}  []models.Address "информация об адресе"
// @Failure      400  {string}  string "400 :Неверный формат запроса"
// @Failure      500  {string}  string "500: Сервис dadata.ru не доступен"
// @Router       /api/address/search [post]
func Search(w http.ResponseWriter, query string) {

	res, err := serviceStore.NewGeoService().Search(query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Println("Ошибка кодирования в JSON", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(resJSON)
	if err != nil {
		log.Println("Ошибка отправки ответа: ", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
