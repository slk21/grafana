package postgresGeo

import (
	"database/sql"
	"log"
	"strconv"
	"studentgit.kata.academy/SLK/repository/internal/models"
)

type AdapterPostgresStore struct {
	db *sql.DB
}

func NewStoreRepository(db *sql.DB) StoreRepository {
	return &AdapterPostgresStore{
		db: db,
	}
}

func (r *AdapterPostgresStore) CreateSearchHistory(request string) (int, error) {
	// Запрос к базе данных search history
	var idSearch int
	err := r.db.QueryRow("INSERT INTO search_history (type_search) VALUES ($1) RETURNING id", request).Scan(&idSearch)
	if err != nil {
		log.Println("Error inserting rows Search_History: ", err)
		return 0, err
	}

	return idSearch, nil
}

func (r *AdapterPostgresStore) CreateAddress(adr models.Address) (int, error) {
	// Запрос к базе данных address
	var idAddress int
	err := r.db.QueryRow("INSERT INTO address (city, street, house, lat, lon) VALUES ($1, $2, $3, $4, $5) RETURNING id", adr.City, adr.Street, adr.House, adr.Lat, adr.Lon).Scan(&idAddress)
	if err != nil {
		log.Println("Error inserting rows Address: ", err)
		return 0, err
	}
	return idAddress, nil
}

func (r *AdapterPostgresStore) CreateHistorySearchAddress(idSearch int, idAddress string) error {
	// Запрос к базе данных history search address
	_, err := r.db.Query("INSERT INTO history_search_address (id_search, id_address) VALUES ($1, $2)", idSearch, idAddress)
	if err != nil {
		log.Println("Error inserting rows History_Search_Address: ", err)
		return err
	}

	return nil
}

func (r *AdapterPostgresStore) GetCoincidences(search string) (map[int]bool, error) {
	rows, err := r.db.Query("SELECT id FROM search_history  WHERE similarity(type_search, $1) > 0.7 ORDER BY similarity(type_search, $1) DESC", search)
	if err != nil {
		log.Printf("Ошибка при получении из БД: %v", err)
		return nil, err
	}

	res := make(map[int]bool)

	for rows.Next() {
		var prodName string
		err = rows.Scan(&prodName)
		if err != nil {
			return nil, err
		}
		id, _ := strconv.Atoi(prodName)
		res[id] = true
	}

	return res, nil
}

func (r *AdapterPostgresStore) GetConnection(id int) ([]int, error) {
	var tagsASCII []uint8

	err := r.db.QueryRow("SELECT id_address FROM history_search_address WHERE id_search = $1", id).Scan(&tagsASCII)
	if err != nil {
		log.Printf("Ошибка при получении ID ответа по ID запроса из БД: %v", err)
		return nil, err
	}

	tags := ASCIItoInt(tagsASCII[1 : len(tagsASCII)-1])

	return tags, nil
}

func (r *AdapterPostgresStore) GetAddress(id []int) ([]models.Address, error) {
	var addresses []models.Address

	for _, valId := range id {
		rows, err := r.db.Query("SELECT city, street, house, lat, lon FROM address WHERE id = $1", valId)
		if err != nil {
			log.Println("Ошибка при выполнении запроса:", err)
			return nil, err
		}
		defer rows.Close()

		// Обработка результатов запроса
		for rows.Next() {
			var address models.Address
			// Сканирование значений из результата запроса в поля структуры Address
			if err = rows.Scan(&address.City, &address.Street, &address.House, &address.Lat, &address.Lon); err != nil {
				log.Println("Ошибка при сканировании результата запроса:", err)
				return nil, err
			}
			// Добавление структуры Address в срез addresses
			addresses = append(addresses, address)
		}

		// Проверка наличия ошибок после обхода результатов запроса
		if err = rows.Err(); err != nil {
			log.Println("Ошибка при обработке результатов запроса:", err)
			return nil, err
		}
	}

	return addresses, nil
}
