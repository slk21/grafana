package postgresGeo

import "studentgit.kata.academy/SLK/repository/internal/models"

type StoreRepository interface {
	CreateSearchHistory(request string) (int, error)
	CreateAddress(adr models.Address) (int, error)
	CreateHistorySearchAddress(idSearch int, idAddress string) error
	GetCoincidences(search string) (map[int]bool, error)
	GetConnection(id int) ([]int, error)
	GetAddress(id []int) ([]models.Address, error)
}
