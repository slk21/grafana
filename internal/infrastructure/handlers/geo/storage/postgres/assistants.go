package postgresGeo

import (
	"fmt"
	"strconv"
)

// ASCIItoInt преобразует ASCII коды цифр в числа, игнорируя запятые
func ASCIItoInt(asciiCodes []uint8) []int {
	var result []int
	var currentNum int
	for _, code := range asciiCodes {
		if code == 44 { // 44 - ASCII код для запятой
			result = append(result, currentNum)
			currentNum = 0
		} else if code >= 48 && code <= 57 { // ASCII коды для цифр от 0 до 9
			currentNum = currentNum*10 + int(code-48)
		}
	}
	result = append(result, currentNum) // Добавляем последнее число
	return result
}

func stringToIntSlice(s []string) []int {
	// Преобразуем строки в целые числа
	var intSlice []int
	for _, num := range s {
		i, err := strconv.Atoi(num)
		if err != nil {
			// Обработка ошибки, если строка не может быть преобразована в число
			fmt.Printf("Ошибка при преобразовании строки %s в число: %v\n", num, err)
			continue
		}
		intSlice = append(intSlice, i)
	}
	return intSlice
}
