package proxy

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	errs "studentgit.kata.academy/SLK/repository/internal/error"
	geoController "studentgit.kata.academy/SLK/repository/internal/infrastructure/handlers/geo/controller"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"studentgit.kata.academy/SLK/repository/internal/storages"
	"time"
)

func Proxy(w http.ResponseWriter, r *http.Request) {

	switch r.URL.Path {
	case "/api/address/search":
		var req models.SearchRequest

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			errs.NewError(500, "не удалось прочитать полученные данные").Error(w)
		}

		log.Println("поиск кэша в REDIS для запроса: ", req.Query)

		start := time.Now() // стартовое время для метрики

		res, errr := storages.Prox.Get(req.Query)

		duration := time.Since(start).Seconds()                                           // вычисление работы запроса
		metrics.RedisDurationSeconds.WithLabelValues("SEARCH_IN_REDIS").Observe(duration) // отправка собранной метрики

		if errr != nil {
			log.Printf("Для запроса %s в REDIS ничего не найдено", req.Query)
			geoController.Search(w, req.Query)
			return
		}

		log.Printf("Для запроса %s использован кэш REDIS", req.Query)

		models.Standart_Header(w)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(res.(string)))
	case "/api/address/geocode":

		var req models.GeoCodeRequest

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			errs.NewError(500, "не удалось прочитать полученные данные").Error(w)
		}

		key := fmt.Sprintf("%.6f_%.6f", req.Lat, req.Lon)

		log.Printf("поиск кэша в REDIS для запроса: %s", key)

		start := time.Now() // стартовое время для метрики

		res, errr := storages.Prox.Get(fmt.Sprintf("%.6f_%.6f", req.Lat, req.Lon))

		duration := time.Since(start).Seconds()                                           // вычисление работы запроса
		metrics.RedisDurationSeconds.WithLabelValues("SEARCH_IN_REDIS").Observe(duration) // отправка собранной метрики

		if errr != nil {
			log.Printf("Для запроса %s в REDIS ничего", key)
			geoController.GeoCod(w, req)
			return
		}

		log.Printf("Для запроса %s использован кэш REDIS", key)

		models.Standart_Header(w)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(res.(string)))
	}
}
