package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type User struct {
	Email      string `json:"email"`
	FirstName  string `json:"firstname"`
	ID         int    `json:"id"`
	LastName   string `json:"lastname"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
	UserStatus int    `json:"user_status"`
	Username   string `json:"username"`
}

func main() {
	// Генерируем случайные данные
	rand.Seed(time.Now().UnixNano())

	// Адрес сервера
	serverURL := "http://localhost:8080/user"

	// Количество запросов
	numRequests := 1000

	for i := 0; i < numRequests; i++ {
		// Генерируем случайные данные для пользователя
		go func() {
			user := []User{{
				Email:      fmt.Sprintf("user%d@example.com", rand.Intn(1000)),
				FirstName:  gofakeit.FirstName(),
				ID:         rand.Intn(1000),
				LastName:   gofakeit.LastName(),
				Password:   gofakeit.Password(true, false, false, false, false, 32),
				Phone:      fmt.Sprintf("+123456789%d", rand.Intn(1000)),
				UserStatus: rand.Intn(2),
				Username:   gofakeit.Username(),
			},
			}

			// Кодируем данные в формат JSON
			jsonData, err := json.Marshal(user)
			if err != nil {
				fmt.Println("Error encoding JSON:", err)
				return
			}

			// Отправляем POST-запрос на сервер
			_, err = http.Post(serverURL, "application/json", bytes.NewBuffer(jsonData))
			if err != nil {
				fmt.Println("Error sending POST request:", err)
				return
			}

			fmt.Println("POST request sent:", user)
		}()
	}

	serverURL = "http://localhost:8080/user/createWithList"
	for i := 0; i < numRequests; i++ {
		// Генерируем случайные данные для пользователя
		go func() {
			user := User{
				Email:      fmt.Sprintf("user%d@example.com", rand.Intn(1000)),
				FirstName:  gofakeit.FirstName(),
				ID:         rand.Intn(1000),
				LastName:   gofakeit.LastName(),
				Password:   gofakeit.Password(true, false, false, false, false, 32),
				Phone:      fmt.Sprintf("+123456789%d", rand.Intn(1000)),
				UserStatus: rand.Intn(2),
				Username:   gofakeit.Username(),
			}

			// Кодируем данные в формат JSON
			jsonData, err := json.Marshal(user)
			if err != nil {
				fmt.Println("Error encoding JSON:", err)
				return
			}

			// Отправляем POST-запрос на сервер
			_, err = http.Post(serverURL, "application/json", bytes.NewBuffer(jsonData))
			if err != nil {
				fmt.Println("Error sending POST request:", err)
				return
			}

			fmt.Println("POST request sent:", user)
		}()
	}

	serverURL = "http://localhost:8080/user/createWithArray"
	for i := 0; i < numRequests; i++ {
		// Генерируем случайные данные для пользователя
		go func() {
			user := User{
				Email:      fmt.Sprintf("user%d@example.com", rand.Intn(1000)),
				FirstName:  gofakeit.FirstName(),
				ID:         rand.Intn(1000),
				LastName:   gofakeit.LastName(),
				Password:   gofakeit.Password(true, false, false, false, false, 32),
				Phone:      fmt.Sprintf("+123456789%d", rand.Intn(1000)),
				UserStatus: rand.Intn(2),
				Username:   gofakeit.Username(),
			}

			// Кодируем данные в формат JSON
			jsonData, err := json.Marshal(user)
			if err != nil {
				fmt.Println("Error encoding JSON:", err)
				return
			}

			// Отправляем POST-запрос на сервер
			_, err = http.Post(serverURL, "application/json", bytes.NewBuffer(jsonData))
			if err != nil {
				fmt.Println("Error sending POST request:", err)
				return
			}

			fmt.Println("POST request sent:", user)
		}()

		// Struct для запроса
		type SearchQuery struct {
			Query string `json:"query"`
		}

		// Bearer токен
		token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6IiQyYSQxMCRMUWNhN2RVdzBXalhLdHdrNzFYSGsubFMxWWx3N2xjRndPOHpjL1VPZHRQU0tRYzQ1LjJScSIsInVzZXJuYW1lIjoic3RyaW5nIn0.eL8dopFtDcp1pcyvnw9ZD-wQluLtoK8k6W0bzrfMjoc"

		// Количество горутин
		numGoroutines := 1000

		// Создание канала для синхронизации горутин
		var wg sync.WaitGroup

		serverURL = "http://localhost:8080/api/address/search"

		// Цикл для запуска горутин
		for i := 0; i < numGoroutines; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()

				// Создание JSON запроса
				query := SearchQuery{Query: fmt.Sprintf("москва арбатская %s", gofakeit.Number(1, 56))}
				jsonData, err := json.Marshal(query)
				if err != nil {
					fmt.Println("Error marshalling JSON:", err)
					return
				}

				// Создание HTTP запроса
				req, err := http.NewRequest("POST", serverURL, bytes.NewBuffer(jsonData))
				if err != nil {
					fmt.Println("Error creating request:", err)
					return
				}

				// Добавление заголовка Authorization с Bearer токеном к запросу
				req.Header.Set("Authorization", "Bearer "+token)
				req.Header.Set("Content-Type", "application/json")

				// Отправка запроса
				client := &http.Client{}
				resp, err := client.Do(req)
				if err != nil {
					fmt.Println("Error sending request:", err)
					return
				}
				defer resp.Body.Close()

				// Чтение ответа
				//body, err := ioutil.ReadAll(resp.Body)
				//if err != nil {
				// fmt.Println("Error reading response:", err)
				// return
				//}

				//fmt.Println("Response:", string(body))
			}()
		}

		// Ожидание завершения всех горутин
		wg.Wait()
	}

	time.Sleep(15 * time.Second)
}
