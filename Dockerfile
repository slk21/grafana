FROM golang:alpine AS builder

EXPOSE 8080

RUN apk add --no-cache git

WORKDIR /serverApp

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -o server ./cmd/main.go

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /serverApp/server .
COPY --from=builder /serverApp/.env ../

# Команда для запуска сервера
CMD ["./server"]