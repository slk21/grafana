package run

import (
	"fmt"
	"github.com/go-chi/jwtauth/v5"
	"github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"studentgit.kata.academy/SLK/repository/config"
	"studentgit.kata.academy/SLK/repository/internal/infrastructure/server"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"studentgit.kata.academy/SLK/repository/internal/storages"
	red "studentgit.kata.academy/SLK/repository/pkg/db/redis"
	"syscall"
	"time"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap() Runner
}

// App структура приложения
type App struct {
	conf config.AppConf
}

func NewApp(conf config.AppConf) *App {
	return &App{conf: conf}
}

func (a *App) Run() error {
	// Создание канала для получения сигналов остановки
	stop := make(chan os.Signal, 1) // буфер используется на случай занятости главной функции(для 100% принятия сигнала)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	var sr *http.Server
	go func() {
		s := server.NewHttpServer(a.conf)
		sr = s.Shutdown()
		err := s.Serve()
		if err != nil {
			log.Fatal(err)
		}
	}()

	<-stop // Ожидание сигнала для остановки сервера

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second) // создание контекста с таймаутом для gracefull shutdown
	defer cancel()

	if err := sr.Shutdown(ctx); err != nil { // Остановка сервера с использованием gracefull shutdown
		return err
	}

	fmt.Print("\r")
	log.Println("Сервер остановлен с помощью Graceful shutdown.")
	return nil
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap() Runner {
	// инициализация подключения к БД PostgreSQL
	storages.NewStorages(a.conf.DB)

	//инициализация redis
	storages.Prox = red.NewClient(a.conf.Redis)

	// инициализация переменной для создания и верификации токена JWT
	models.TokenAuth = jwtauth.New("HS256", []byte(a.conf.TokenSecret), nil)

	// регистрация метрик
	prometheus.MustRegister(metrics.HttpRequestsTotal)      // метрика количества запросов по ручкам
	prometheus.MustRegister(metrics.HandlerDurationSeconds) // метрика времени выполнения ручки
	prometheus.MustRegister(metrics.BdDurationSeconds)      // метрика выполнения запроса к postgres
	prometheus.MustRegister(metrics.RedisDurationSeconds)   // метрика выполнения запроса к redis
	prometheus.MustRegister(metrics.DadataDurationSeconds)  // метрика запросов к Dadata.ru

	return a
}
