package redis

import (
	"context"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
)

func (c *client) Set(key string, value interface{}) error {
	ctx := context.Background()
	// установка подключения к Redis
	err := ping(ctx, c.client)
	if err != nil {
		return err
	}

	c.client.Set(ctx, key, value, 0)
	return nil
}

func (c *client) Get(key string) (interface{}, error) {
	ctx := context.Background()
	// установка подключения к Redis
	err := ping(ctx, c.client)
	if err != nil {
		return nil, err
	}

	res, err := c.client.Get(ctx, key).Result()
	if err == redis.Nil {
		return nil, fmt.Errorf("not found by key %%%s%%", key)
	} else if err != nil {
		return nil, errors.New("ошибка при получении данных из кэша по ключу: " + err.Error())
	}

	return res, nil
}

// Устанавливает подключение с Redis.
// Вернёт ошибку если подключение не произошло с описанием ошибки.
func ping(ctx context.Context, client *redis.Client) error {
	_, err := client.Ping(ctx).Result()
	if err != nil {
		return errors.New("ошибка при подключении к Redis: " + err.Error())
	}
	return nil
}
