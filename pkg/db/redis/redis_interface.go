package redis

import (
	"github.com/redis/go-redis/v9"
	"studentgit.kata.academy/SLK/repository/config"
)

type Cacher interface {
	Set(key string, value interface{}) error
	Get(key string) (interface{}, error)
}

type client struct {
	client *redis.Client
}

func NewClient(conf config.Redis) Cacher {
	return &client{
		client: redis.NewClient(&redis.Options{
			Addr: conf.Host + ":" + conf.Port,
		}),
	}
}
