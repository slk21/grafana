CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE users (
      id          SERIAL       PRIMARY KEY,
      username    VARCHAR(50)  NOT NULL      UNIQUE,
      firstname   VARCHAR(50)  NOT NULL,
      lastname    VARCHAR(50)  NOT NULL,
      email       VARCHAR(100) NOT NULL,
      password    VARCHAR(100) NOT NULL,
      phone       VARCHAR(20)  NOT NULL,
      user_status INT          NOT NULL
);

CREATE TABLE search_history (
	id           SERIAL    PRIMARY KEY,
	type_search            VARCHAR(20)
);

CREATE TABLE history_search_address(
    id_search  INT  NOT NULL,
    id_address INT[]  NOT NULL
);

CREATE TABLE address (
    id SERIAL  PRIMARY KEY,
    city   VARCHAR,
    street VARCHAR,
    house  VARCHAR,
    lat    VARCHAR,
    lon    VARCHAR
);


