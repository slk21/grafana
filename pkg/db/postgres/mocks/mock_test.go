package mocks

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"studentgit.kata.academy/SLK/repository/pkg/db/postgres"
	"testing"
)

func TestMockUserRepository_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockUserRepository(ctrl)

	// Устанавливаем ожидаемый результат
	expectedError := errors.New("error message")
	mockRepo.EXPECT().Create(gomock.Any()).Return(expectedError)

	// Вызываем метод, который мы хотим протестировать
	err := mockRepo.Create(postgres.UserDB{})

	// Проверяем, что результат соответствует ожидаемому
	assert.Equal(t, expectedError, err)
}

func TestMockUserRepository_Delete(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockUserRepository(ctrl)

	// Устанавливаем ожидаемый результат
	expectedError := errors.New("error message")
	mockRepo.EXPECT().Delete(gomock.Any()).Return(expectedError)

	// Вызываем метод, который мы хотим протестировать
	err := mockRepo.Delete("some_id")

	// Проверяем, что результат соответствует ожидаемому
	assert.Equal(t, expectedError, err)
}

func TestMockUserRepository_Update(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockUserRepository(ctrl)

	// Устанавливаем ожидаемый результат
	expectedError := errors.New("error message")
	mockRepo.EXPECT().Update(gomock.Any()).Return(expectedError)

	// Вызываем метод, который мы хотим протестировать
	err := mockRepo.Update(postgres.UserDB{})

	// Проверяем, что результат соответствует ожидаемому
	assert.Equal(t, expectedError, err)
}
