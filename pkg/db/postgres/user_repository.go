package postgres

import (
	"database/sql"
	"errors"
	"log"
)

func NewUserRepository(db *sql.DB) UserRepository {
	return &AdapterPostgres{
		db: db,
	}
}

// Create добавляет в таблицу нового пользователя
func (r *AdapterPostgres) Create(sqlRequest string) error {
	_, err := r.db.Exec(sqlRequest)
	if err != nil {
		log.Printf("Ошибка при добавлении питомца в БД: %v", err)
		return err
	}
	return nil
}

// GetByUsername возвращает значения пользователя
func (r *AdapterPostgres) GetByUsername(sqlRequest, userType string) (interface{}, error) {

	row := r.db.QueryRow(sqlRequest)

	switch userType {
	case "users":
		var us UsersDB
		err := row.Scan(&us.Id, &us.Username, &us.Firstname, &us.Lastname, &us.Email, &us.Password, &us.Phone, &us.UserStatus)
		if err != nil {
			return us, err
		}
		return us, nil
	}

	return nil, errors.New("подходящей таблицы для поиска значений не найдено")
}

func (r *AdapterPostgres) Update(sqlRequest string) error {
	_, err := r.db.Exec(sqlRequest)
	if err != nil {
		log.Printf("Ошибка при обновлении питомца в БД: %v", err)
		return err
	}
	return nil
}

func (r *AdapterPostgres) Delete(sqlRequest string) error {
	_, err := r.db.Exec(sqlRequest)
	if err != nil {
		return err
	}
	return nil
}

// List возвращает список пользователей с указанием количества пользователей в ответе и пагинацией (параметры limit и offset)
func (r *AdapterPostgres) List(sqlRequest, nameStruct string) (interface{}, error) {

	return nil, nil
}
