package postgres

import "database/sql"

type AdapterPostgres struct {
	db *sql.DB
}

// UserRepository интерфейс действий для работы с БД postgres
// Create метод создания значений в таблицах
// GetByUsername
type UserRepository interface {
	Create(sqlRequest string) error
	GetByUsername(sqlRequest, userType string) (interface{}, error)
	Update(sqlRequest string) error
	Delete(sqlRequest string) error
	List(sqlRequest, nameStruct string) (interface{}, error)
	// Другие методы, необходимые для работы с пользователями
}

type Conditions struct {
	Limit  int `json:"limit"`
	Offset int `json:"offset"`
}

// UsersDB структура пользователя для таблицы users
type UsersDB struct {
	Id         int64  `json:"id"`
	Username   string `json:"username"`
	Firstname  string `json:"firstname"`
	Lastname   string `json:"lastname"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
	UserStatus int32  `json:"user_status"`
}

// PetDB структура питомца для таблицы pet
type PetDB struct {
	ID       int64      `json:"id"`
	Category CategoryDB `json:"category"`
	Name     string     `json:"name"`
	Tags     []TagDB    `json:"tags"`
	Status   string     `json:"status"`
}

// CategoryDB структура питомца для таблицы category
type CategoryDB struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// TagDB структура питомца для таблицы tag
type TagDB struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
