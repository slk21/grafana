package postgres

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"studentgit.kata.academy/SLK/repository/config"
)

func OpenDB(dbconfig config.DB) (*sql.DB, error) {
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", dbconfig.User, dbconfig.Password, dbconfig.Host, dbconfig.Port, dbconfig.Name)
	db, err := sql.Open(dbconfig.Driver, connectionString)
	if err != nil {
		fmt.Println("Error connecting to database")
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}
