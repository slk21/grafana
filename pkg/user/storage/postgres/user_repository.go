package postgresUser

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"studentgit.kata.academy/SLK/repository/internal/models"
	db "studentgit.kata.academy/SLK/repository/pkg/db/postgres"
)

func NewUserDB(user models.User) (UserDB, error) {
	return UserDB{
		Id:         user.Id,
		Username:   user.Username,
		Firstname:  user.Firstname,
		Lastname:   user.Lastname,
		Email:      user.Email,
		Password:   user.Password,
		Phone:      user.Phone,
		UserStatus: user.UserStatus,
	}, nil
}

func converterUserDB(user db.UsersDB) (UserDB, error) {
	return UserDB{
		Id:         user.Id,
		Username:   user.Username,
		Firstname:  user.Firstname,
		Lastname:   user.Lastname,
		Email:      user.Email,
		Password:   user.Password,
		Phone:      user.Phone,
		UserStatus: user.UserStatus,
	}, nil
}

type AdapterPostgres struct {
	db        *sql.DB
	tableName string
}

func NewUserRepository(db *sql.DB, tName string) UserRepository {
	return &AdapterPostgres{
		db:        db,
		tableName: tName,
	}
}

func (r *AdapterPostgres) GetByUsername(username string) (UserDB, error) {
	// создание запроса на получение данных о пользователе по его username
	dbGetUsernameInfo := fmt.Sprintf("SELECT * FROM %s WHERE username = '%s'", r.tableName, username)

	// получение интерфейса БД и отправка данных на метод GetByUsername
	userDB, err := db.NewUserRepository(r.db).GetByUsername(dbGetUsernameInfo, "users")
	if err != nil {
		return UserDB{}, err
	}

	// проверка полученной от внешнего слоя репозитория значения на соответствие типу
	switch userDB.(type) {
	case db.UsersDB:
		convertedUserDB, _ := converterUserDB(userDB.(db.UsersDB))
		return convertedUserDB, nil
	default:
		log.Printf("тип вернувшийся из репозитория не является UserDB. Полученный тип: %T\n", userDB)
		return UserDB{}, errors.New("возвращенное значение из таблицы имеет неподходящий тип")
	}
}

func (r *AdapterPostgres) Update(username string, user UserDB) error {
	dbUpdateInfo := fmt.Sprintf("UPDATE %s SET username = '%s', firstname = '%s', lastname = '%s', email = '%s', password = '%s', phone = '%s', user_status = '%d' WHERE username = '%s' ", r.tableName, user.Username, user.Firstname, user.Lastname, user.Email, user.Password, user.Phone, user.UserStatus, username)

	err := db.NewUserRepository(r.db).Update(dbUpdateInfo)
	if err != nil {
		return err
	}

	return nil

}

func (r *AdapterPostgres) Delete(username string) error {
	// создание запроса для удаления пользователя
	dbDeleteInfo := fmt.Sprintf("DELETE FROM %s WHERE username = '%s'", r.tableName, username)

	err := db.NewUserRepository(r.db).Delete(dbDeleteInfo)
	if err != nil {
		return err
	}

	return nil
}

func (r *AdapterPostgres) List(condition db.Conditions) ([]UserDB, error) {

	return nil, nil
}

func (r *AdapterPostgres) Create(user UserDB) error {
	// создание запроса на добавление пользователя
	dbCreateInfo := fmt.Sprintf("INSERT INTO %s (username, firstname, lastname, email, password, phone, user_status) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d)", r.tableName, user.Username, user.Firstname, user.Lastname, user.Email, user.Password, user.Phone, user.UserStatus)
	// получение интерфейса БД и отправка данных на метод Create
	err := db.NewUserRepository(r.db).Create(dbCreateInfo)
	if err != nil {
		return err
	}

	return nil
}
