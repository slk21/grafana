package postgresUser

import (
	db "studentgit.kata.academy/SLK/repository/pkg/db/postgres"
)

type UserRepository interface {
	GetByUsername(username string) (UserDB, error)
	Update(username string, user UserDB) error
	Delete(username string) error
	List(condition db.Conditions) ([]UserDB, error)
	Create(user UserDB) error
}

// UserDB содержит информацию о пользователе из базы данных
type UserDB struct {
	Id         int64  `json:"id"`
	Username   string `json:"username"`
	Firstname  string `json:"firstname"`
	Lastname   string `json:"lastname"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	Phone      string `json:"phone"`
	UserStatus int32  `json:"user_status"`
}
