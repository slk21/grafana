package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"log"
	"net/http"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"studentgit.kata.academy/SLK/repository/pkg/user/service"
)

// CreateWithList обновление данных.
// @Summary Creates list of users with given input array
// @Tags user
// @Param body body  []models.User  true "List of user object".
// @Response default "successful operation"
// @Router /user/createWithList [post]
func CreateWithList(w http.ResponseWriter, r *http.Request) {
	var user []models.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400: неверные данные JSON."))
		return
	}

	for _, val := range user {
		// получение интерфейса сервиса и отправка в метод Create полученных данных
		code, err1 := service.NewUserService("users").Create(val)
		if err1 != nil {
			w.WriteHeader(code)
			_, errW := w.Write([]byte(err1.Error()))
			if errW != nil {
				log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
			}
			return
		}

	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: "ok",
	}
	json.NewEncoder(w).Encode(res)
}

// GetByUsername получение пользователя по его username.
// @Summary Get user by user name
// @Tags user
// @Param username path string true "The name that needs to be fetched. Use user1 for testing.".
// @Success      200  {object}  models.User "successful operation"
// @Router /user/{username} [get]
func GetByUsername(w http.ResponseWriter, r *http.Request) {

	// получение имени пользователя из запроса
	username := chi.URLParam(r, "username")

	// получение интерфейса сервиса и использование метода для получения пользователя по его Username
	userDB, code, err := service.NewUserService("users").GetByUsername(username)
	if err != nil {
		w.WriteHeader(code)
		_, errW := w.Write([]byte(err.Error()))
		if errW != nil {
			log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
		}
		return
	}

	// отправка ответа клиенту в формате JSON
	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(userDB)
	if err != nil {
		log.Printf("Ошибка при отправке ответа пользователю %d ", err)
	}
}

// UpdateByUsername обновление данных.
// @Summary Updated user
// @Tags user
// @Description This can only be done by the logged in user.
// @Param username path string true "name that need to be updated".
// @Param body body  models.User  true "List of user object".
// @Router /user/{username} [put]
func UpdateByUsername(w http.ResponseWriter, r *http.Request) {

	// получение имени пользователя из запроса
	username := chi.URLParam(r, "username")

	var user models.User // хранилище полученных из тела данных

	// декодирование полученных JSON данных в хранилище
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400: неверные данные JSON."))
		return
	}

	code, err := service.NewUserService("users").Update(username, user)
	if err != nil {
		w.WriteHeader(code)
		_, errW := w.Write([]byte(err.Error()))
		if errW != nil {
			log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
		}
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: fmt.Sprintf("ok"),
	}
	json.NewEncoder(w).Encode(res)

}

// DeleteByUsername удаление пользователя по его username.
// @Summary Delete user
// @Tags user
// @Description This can only be done by the logged in user.
// @Param username path string true "The name that needs to be deleted".
// @Router /user/{username} [delete]
func DeleteByUsername(w http.ResponseWriter, r *http.Request) {

	// получение имени пользователя из запроса
	username := chi.URLParam(r, "username")

	code, err := service.NewUserService("users").Delete(username)
	if err != nil {
		w.WriteHeader(code)
		_, errW := w.Write([]byte(err.Error()))
		if errW != nil {
			log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
		}
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: fmt.Sprintf(username),
	}
	json.NewEncoder(w).Encode(res)
}

// Login авторизация пользователя по username и password с отправкой токена доступа в ответе.
// @Summary Logs user into the system
// @Tags user
// @Param username query string true "The user name for login"
// @Param password query string true "The password for login in clear text"
// @Success      200  {string}  string "successful operation"
// @Router /user/login [get]
func Login(w http.ResponseWriter, r *http.Request) {

	username := r.URL.Query().Get("username")
	password := r.URL.Query().Get("password")

	token, code, err := service.NewUserService("users").Login(username, password)

	if err != nil {
		http.Error(w, err.Error(), code)
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: fmt.Sprintf("token: %s", token),
	}
	json.NewEncoder(w).Encode(res)
}

// CreateWithArray обновление данных.
// @Summary Creates list of users with given input array
// @Tags user
// @Param body body  []models.User  true "List of user object".
// @Response default "successful operation"
// @Router /user/createWithArray [post]
func CreateWithArray(w http.ResponseWriter, r *http.Request) {

	var user []models.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400: неверные данные JSON."))
		return
	}

	for _, val := range user {
		// получение интерфейса сервиса и отправка в метод Create полученных данных
		code, err1 := service.NewUserService("users").Create(val)
		if err1 != nil {
			w.WriteHeader(code)
			_, errW := w.Write([]byte(err1.Error()))
			if errW != nil {
				log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
			}
			return
		}

	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: "ok",
	}
	json.NewEncoder(w).Encode(res)
}

// CreateUser добавление пользователя.
// @Summary Create user
// @Tags user
// @Description This can only be done by the logged in user.
// @Param input body models.User true "Created user object".
// @Response default "successful operation"
// @Router /user [post]
func CreateUser(w http.ResponseWriter, r *http.Request) {

	var user models.User // хранилище полученных из тела данных

	// декодирование полученных JSON данных в хранилище
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400: неверные данные JSON."))
		return
	}

	// получение интерфейса сервиса и отправка в метод Create полученных данных
	code, err := service.NewUserService("users").Create(user)
	if err != nil {
		w.WriteHeader(code)
		_, errW := w.Write([]byte(err.Error()))
		if errW != nil {
			log.Printf("Ошибка при отправке ответа пользователю %d ", errW)
		}
		return
	}

	models.Standart_Header(w)
	w.WriteHeader(http.StatusOK)
	res := models.ApiResponse{
		Code:    http.StatusOK,
		Type:    "unknown",
		Message: "ok",
	}
	json.NewEncoder(w).Encode(res)
}
