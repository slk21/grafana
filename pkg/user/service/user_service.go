package service

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"log"
	"studentgit.kata.academy/SLK/repository/internal/metrics"
	"studentgit.kata.academy/SLK/repository/internal/models"
	"studentgit.kata.academy/SLK/repository/internal/storages"
	postgresUser "studentgit.kata.academy/SLK/repository/pkg/user/storage/postgres"
	"time"
)

type UserService struct {
	storage postgresUser.UserRepository
}

func NewUserService(nameTable string) Userer {
	return &UserService{
		storage: postgresUser.NewUserRepository(storages.PostgreSQL, nameTable),
	}
}

// Create проводит проверку поученных данных на валидность и отправляет на создание
func (u *UserService) Create(user models.User) (int, error) {

	code, err, userDB := ValidAndHashPass(user)
	if err != nil {
		return code, err
	}

	start := time.Now() // стартовое время для метрики

	err = u.storage.Create(userDB) // отправка данных на внутренний слой репозитория

	duration := time.Since(start).Seconds()                               // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("CREATE").Observe(duration) // отправка собранной метрики

	if err != nil {
		return 500, errors.New("500: ошибка при добавлении пользователя в таблицу")
	}

	return 0, nil
}

func (u *UserService) GetByUsername(username string) (postgresUser.UserDB, int, error) {

	start := time.Now() // стартовое время для метрики

	userDB, err := u.storage.GetByUsername(username) // отправка данных на внутренний слой репозитория

	duration := time.Since(start).Seconds()                            // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("GET").Observe(duration) // отправка собранной метрики

	if err != nil {
		log.Printf("ошибка при получении поьзователя: %v", err)
		return userDB, 500, errors.New("500: ошибка при получении пользователя по username")
	}

	return userDB, 0, nil
}

func (u *UserService) Delete(username string) (int, error) {
	start := time.Now() // стартовое время для метрики

	err := u.storage.Delete(username)

	duration := time.Since(start).Seconds()                               // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("DELETE").Observe(duration) // отправка собранной метрики

	if err != nil {
		log.Printf("ошибка при удалении поьзователя: %v", err)
		return 500, errors.New("500: ошибка при удалении пользователя по username")
	}
	return 0, nil
}

func (u *UserService) Update(username string, user models.User) (int, error) {

	code, err, userDB := ValidAndHashPass(user)
	if err != nil {
		return code, err
	}

	start := time.Now() // стартовое время для метрики

	err = u.storage.Update(username, userDB) // отправка данных на внутренний слой репозитория

	duration := time.Since(start).Seconds()                               // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("UPDATE").Observe(duration) // отправка собранной метрики

	if err != nil {
		return 500, errors.New("500: ошибка при добавлении пользователя в таблицу")
	}

	return 0, nil
}

func (u *UserService) Login(username, password string) (string, int, error) {

	start := time.Now() // стартовое время для метрики

	userDB, err := u.storage.GetByUsername(username)

	duration := time.Since(start).Seconds()                              // вычисление работы запроса
	metrics.BdDurationSeconds.WithLabelValues("LOGIN").Observe(duration) // отправка собранной метрики

	if err != nil {
		return "", 500, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(userDB.Password), []byte(password))
	if err != nil {
		return "", 0, errors.New("Неверный пароль.")
	}

	_, token, _ := models.TokenAuth.Encode(map[string]interface{}{"username": userDB.Username, "password": userDB.Password})

	return token, 0, nil
}

// ValidAndHashPass проверяет пришедшие данные на соответствие требованиям, кодирует пароль, конвертирует пользовательскую БД в БД репозитория
func ValidAndHashPass(user models.User) (int, error, postgresUser.UserDB) {
	// проверка полученных данных на соответствие требованием БД
	if user.Username == "" {
		log.Printf("Пользователь %s %s при добавлении данных оставил поле Username пустым\n", user.Firstname, user.Lastname)
		return 400, errors.New("400: поле Username не может быть пустым"), postgresUser.UserDB{}
	} else if user.Firstname == "" {
		log.Printf("Пользователь c именем пользовтеля %s при добавлении данных оставил поле Firstname пустым\n", user.Username)
		return 400, errors.New("400: поле Firstname не может быть пустым"), postgresUser.UserDB{}
	} else if user.Lastname == "" {
		log.Printf("Пользователь c именем пользовтеля %s при добавлении данных оставил поле Lastname пустым\n", user.Username)
		return 400, errors.New("400: поле Lastname не может быть пустым"), postgresUser.UserDB{}
	} else if user.Email == "" {
		log.Printf("Пользователь %s %s при добавлении данных оставил поле Email пустым\n", user.Firstname, user.Lastname)
		return 400, errors.New("400: поле Email не может быть пустым"), postgresUser.UserDB{}
	} else if user.Password == "" {
		log.Printf("Пользователь %s %s при добавлении данных оставил поле Password пустым\n", user.Firstname, user.Lastname)
		return 400, errors.New("400: поле Password не может быть пустым"), postgresUser.UserDB{}
	} else if user.Phone == "" {
		log.Printf("Пользователь %s %s при добавлении данных оставил поле Phone пустым\n", user.Firstname, user.Lastname)
		return 400, errors.New("400: поле Phone не может быть пустым"), postgresUser.UserDB{}
	}

	// шифрование пароля
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		// http.Error(w, err.Error(), http.StatusBadRequest)
		log.Printf("Ошибка при шифровании пароля пользователя: %v", err)
		return 500, errors.New("500: ошибка при шифровании пароля"), postgresUser.UserDB{}
	}

	user.Password = string(hashedPassword)

	// конвертация пользовательской структуры в структуру БД
	userDB, err := postgresUser.NewUserDB(user)
	if err != nil {
		log.Printf("Ошибка при конвертация пользовательской структуры в структуру БД: %v", err)
		return 500, errors.New("500: ошибка при обработке данных запроса"), postgresUser.UserDB{}
	}

	return 0, nil, userDB
}
