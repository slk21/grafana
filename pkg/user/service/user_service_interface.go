package service

import (
	"studentgit.kata.academy/SLK/repository/internal/models"
	postgresUser "studentgit.kata.academy/SLK/repository/pkg/user/storage/postgres"
)

type Userer interface {
	Create(user models.User) (int, error)
	GetByUsername(username string) (postgresUser.UserDB, int, error)
	Delete(username string) (int, error)
	Update(username string, user models.User) (int, error)
	Login(username, password string) (string, int, error)
}
