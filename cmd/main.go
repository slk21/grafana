package main

import (
	"studentgit.kata.academy/SLK/repository/config"
	_ "studentgit.kata.academy/SLK/repository/internal/docs"
	"studentgit.kata.academy/SLK/repository/run"
)

// @title Swagger Ge-service
// @version 1.0.0
// @description
// @contact.name the developer
// @contact.email slavalomov.4@gmail.com
// @host localhost:8080
// @BasePath /
// @accept json
// @produce json
// @securitydefinitions.apikey JWTAuth
// @in header
// @name Authorization
func main() {
	app := run.NewApp(config.NewAppConf())

	err := app.Bootstrap().Run()
	if err != nil {
		panic(err)
	}

}
